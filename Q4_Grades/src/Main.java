/*Q4. Write a program to calculate the grade of a student. There are five
        subjects. Marks in each subject are entered from keyboard. Assign grade
        based on the following rule:
        Total Marks >= 90 Grade: Ex
        90 > Total Marks >= 80 Grade: A
        80 > Total Marks >= 70 Grade: B
        70 > Total Marks >= 60 Grade: C
        60 > Total Marks Grade: F*/

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        boolean flag = false;
        while (!flag) {
            System.out.println("Enter the marks of student");
            int marks = scanner.nextInt();

            if(marks<100 &&marks>=90){
                System.out.println("Grade = Ex");
            }else if (marks < 90 && marks >= 80) {
                System.out.println(" Grade = A ");
            } else if (marks < 80 && marks >= 70) {
                System.out.println("Grade = B");
            } else if (marks < 70 && marks >= 60) {
                System.out.println("Grade = C");
            } else if (marks < 60) {
                System.out.println("Grade = F");
            }else if(marks>100){
                flag=true;
            }
            System.out.println("TO exit the loop press 100 key");


        }
    }
}