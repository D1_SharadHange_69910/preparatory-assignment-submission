//Q9. Accept an integer number and when the program is executed print the
//binary, octal and hexadecimal equivalent of the given number.

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a Integer number");
        int number = scanner.nextInt();

        System.out.println("Binary Equivalent : "+Integer.toBinaryString(number));
        System.out.println("Octal Equivalent : "+Integer.toOctalString(number));
        System.out.println("Hexadecimal Equivalent : "+Integer.toHexString(number));

       /* int newNum = number;
        int [] binaryNumber = new int [10];
        for(int i = 0; i<number ; i++){

             binaryNumber[i] = newNum % 2;
             newNum = newNum/2;
            System.out.print(binaryNumber[i]);
            */






    }
}