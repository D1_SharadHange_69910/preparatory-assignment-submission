//Q7. Write a program to accept a number from user using command line argument and display its table.

public class Main {
    public static void main(String[] args) {
        int number = Integer.parseInt(args[0]);
        int table = 1;
        for(int i = 1 ; i<=10 ; i++){
            table = number * i;
            System.out.println(table);
        }
    }
}