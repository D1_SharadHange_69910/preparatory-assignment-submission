
//Q2. Write a program to calculate a Factorial of a number.

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number");
        int factorial = 1;
        int number = scanner.nextInt();
        for(int i = number; i>0 ; i--){
            factorial = factorial*i;
        }

        System.out.println("Factorial of number is " + factorial);
    }
}