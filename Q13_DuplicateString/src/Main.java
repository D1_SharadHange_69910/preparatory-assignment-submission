public class Main {
    public static void main(String[] args) {
      /*  Q13. Declare an Array of type String. Display the strings which are
        duplicated in that array. (Hint: use equals())
        */

        String [] Array = { "abc" , "abc", "Sharad", "Sharad" , "hange", "Munde"};

        for(int i = 0; i<Array.length-1 ; i++){

           for(int j = i+1; j<Array.length ; j++){
            if(Array[i].equals(Array[j]) && i !=j) {
                System.out.println("Duplicate element is " + Array[j]);
            }
            }
        }
    }
}