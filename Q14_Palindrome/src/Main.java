import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Q14 Write a program to check if give string is palindrome.

        System.out.println("Enter a String");
        Scanner scanner = new Scanner(System.in);

        String input = scanner.next();
        char []ch = new char[input.length()];
        String reverse = "";
        for(int i = 0; i<input.length() ; i++){
            ch[i] = input.charAt(i);
            reverse = ch[i] + reverse;

        }
        System.out.println(reverse);
        if(input.equals(reverse)){
            System.out.println("Given string is palindrome");
        }else {
            System.out.println("Given string is not palindrome");
        }
    }
}