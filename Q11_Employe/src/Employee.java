public class Employee {
/*    Q11. Create a class called Employee that includes three fields - a first name
            (type String), a last name (type String) and a monthly salary (double).
    Provide a constructor that initializes these fields. Provide a set and a get
    method for each instance variable. If the monthly salary is not positive, do
    not set its value.
    */
 private String name;
 private String lastName;
 private double monthlySalary;

 public Employee(String name, String lastName, double monthlySalary){
     this.name = name;
     this.lastName = lastName;
     this.monthlySalary = monthlySalary;
 }

 public void setName(String name){
     this.name = name;
 }

 public void  setLastName(String lastName){
     this.lastName = lastName;
 }

    public Employee() {
    }

    public void setMonthlySalary(double monthlySalary){
     if(monthlySalary>0){
         this.monthlySalary = monthlySalary;
     }else {
         System.out.println("Salary is negative");
     }
 }

 public void increment(int percentage){
     double percentagetoIncome = percentage*this.monthlySalary;
     this.monthlySalary += percentagetoIncome;
 }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public double getMonthlySalary() {
        return monthlySalary;
    }
}
