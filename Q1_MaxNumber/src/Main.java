

//Q1. Write a program to input n numbers on command line argument and calculate maximum of them.

public class Main {
    public static void main(String[] args) {
       int n = args.length;
       int maxNumber = 0;

        for (int i = 0; i<n ; i++){
            maxNumber = Integer.parseInt(args[i]);
            if(Integer.parseInt(args[i])<Integer.parseInt(args[++i])){
                maxNumber = Integer.parseInt(args[i]);
            }
        }

        System.out.println("Maximum number is "+ maxNumber);
    }
}