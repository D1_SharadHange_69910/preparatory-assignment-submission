/*
Q8. Write a program to read the name of a student (studentName), roll
        Number (rollNo) and marks (totalMarks) obtained. rollNo may be an
        alphanumeric string. Display the data as read. Hint: Create a Student class.
        */


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Student student = new Student();
        System.out.println("Enter Name, roll no and total Marks of student");

        String name = scanner.next();
        String rollNo = scanner.next();
        int marks = scanner.nextInt();

        student.setName(name);
        student.setRollNo(rollNo);
        student.setMarks(marks);


    }


}
