import java.util.Scanner;
/*Q5. Write a program to check the input characters for uppercase,
lowercase, number of digits and other characters. Display appropriate
messag*/
public class Main {
    public static void main(String[] args) {
        System.out.println("Enter your input");
        Scanner scanner = new Scanner(System.in);
        char ch = scanner.next().charAt(0);
        int number = (int)ch;

        int choice = -1 ;
        if(number>=65 && number<=90){
            choice = 0;
        }else if(number>=97 && number<=122){
            choice=1;
        }else if(number>=48 && number<=57){
            choice = 2;
        }

        switch (choice){
            case 0:
                System.out.println("Your input is UpperCase");
                break;
            case 1:
                System.out.println("Your input is LoweCase");
                break;
            case 2:
                System.out.println("Your input is number");
                break;
            default:
                System.out.println("Your input is special charater");
        }
    }
}