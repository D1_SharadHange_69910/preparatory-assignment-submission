import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Q12. Write a Program to reverse the letters present in the given String.

        System.out.println("Enter the name which you want to reverse");

        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();
        System.out.println("Original input : " + input);
        String rev = "";
        char ch;
for(int i = 0; i<input.length() ; i++){
ch = input.charAt(i);
rev = ch + rev;
}

        System.out.println("Reverse input :" + rev);


    }
}