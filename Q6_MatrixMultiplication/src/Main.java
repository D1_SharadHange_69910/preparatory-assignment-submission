//Q6. Write a program to perform matrix multiplication.

public class Main {
    public static void main(String[] args) {
        int [][] Array1 = {{1,1,1},{2,2,2},{3,3,3}};
        int [][] Array2 = {{1,1,1},{2,2,2},{3,3,3}};

        int [][] Array3 = new int[3][3];

        for(int i = 0; i<3; i++){
            for(int j = 0; j<3 ; j++){
                Array3[i][j] = 0;

                for(int k= 0; k<3 ; k++){
                    Array3[i][j] += Array1[i][k] * Array2[k][j];
                }
                System.out.println(Array3[i][j]);
            }
        }
    }
}