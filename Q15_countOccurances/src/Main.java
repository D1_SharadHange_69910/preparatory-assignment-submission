import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Q15. Input a string from the user. Count occurrences (case insensitive) of
        //each alphabet in the string.

        System.out.println("Enter a string");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();
        input = input.toUpperCase();


        char []ch = new char[input.length()];

        int countA=0, countB=0, countC=0, countD=0, countE=0, countF=0, countG = 0, countH= 0, countI=0, countJ=0, countK=0, countL = 0;
        int countM = 0, countN=0, countO=0, countP=0, countQ=0, countR=0, countS= 0, countT=0, countU=0, countV=0, countW=0, countX= 0;
        int countY=0, countZ=0;

        for(int i = 0; i<input.length() ; i++){
            ch[i] = input.charAt(i);
            if(ch[i]=='A'){
                countA++;
            } else if (ch[i]=='B') {
                countB++;
            } else if (ch[i]=='C') {
                countC++;
            } else if (ch[i]=='D') {
                countD++;
            }else if (ch[i]=='E') {
                countE++;
            }else if (ch[i]=='F') {
                countF++;
            }else if (ch[i]=='G') {
                countG++;
            }else if (ch[i]=='H') {
                countH++;
            }else if (ch[i]=='I') {
                countI++;
            }else if (ch[i]=='J') {
                countJ++;
            }else if (ch[i]=='K') {
                countK++;
            }else if (ch[i]=='L') {
                countL++;
            }else if (ch[i]=='M') {
                countM++;
            }else if (ch[i]=='N') {
                countN++;
            }else if (ch[i]=='O') {
                countO++;
            }else if (ch[i]=='P') {
                countP++;
            }else if (ch[i]=='Q') {
                countQ++;
            }else if (ch[i]=='R') {
                countR++;
            }else if (ch[i]=='S') {
                countS++;
            }else if (ch[i]=='T') {
                countT++;
            }else if (ch[i]=='U') {
                countU++;
            }else if (ch[i]=='V') {
                countV++;
            }else if (ch[i]=='W') {
                countW++;
            }else if (ch[i]=='X') {
                countX++;
            }else if (ch[i]=='Y') {
                countY++;
            }else if (ch[i]=='Z') {
                countZ++;
            }


        }

        if(countA>0){
            System.out.println("A : " + countA);
        }
        if(countB>0){
            System.out.println("B : " + countB);
        }
        if(countC>0){
            System.out.println("C : " + countC);
        }
        if(countD>0){
            System.out.println("D : " + countD);
        }
        if(countE>0){
            System.out.println("E : " + countE);
        }
        if(countF>0){
            System.out.println("F : " + countF);
        }
        if(countG>0){
            System.out.println("G : " + countG);
        }
        if(countH>0){
            System.out.println("H : " + countH);
        }
        if(countI>0){
            System.out.println("I : " + countI);
        }if(countJ>0){
            System.out.println("J : " + countJ);
        }if(countK>0){
            System.out.println("K : " + countK);
        }
        if(countL>0){
            System.out.println("L : " + countL);
        }
        if(countM>0){
            System.out.println("M : " + countM);
        }
        if(countN>0){
            System.out.println("N : " + countN);
        }if(countO>0){
            System.out.println("O : " + countO);
        }
        if(countP>0){
            System.out.println("P : " + countP);
        }
        if(countQ>0){
            System.out.println("Q : " + countQ);
        }
        if(countR>0){
            System.out.println("R : " + countR);
        }
        if(countS>0){
            System.out.println("S : " + countS);
        }if(countT>0){
            System.out.println("T : " + countT);
        }
        if(countU>0){
            System.out.println("U : " + countU);
        }
        if(countV>0){
            System.out.println("V : " + countV);
        }
        if(countW>0){
            System.out.println("W : " + countW);
        }
        if(countX>0){
            System.out.println("X : " + countX);
        }
        if(countY>0){
            System.out.println("Y : " + countY);
        }
        if(countZ>0){
            System.out.println("Z : " + countZ);
        }




    }
}