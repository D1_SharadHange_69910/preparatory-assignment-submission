//Q3. Write a program to calculate Fibonacci Series up to n numbers

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number");
        int n = scanner.nextInt();
        int firstTerm = 0;
        int secondTerm = 1;
        for(int i = 0; i<=n ; i++){
            System.out.println(firstTerm);
            int nextTerm = firstTerm + secondTerm;
            firstTerm = secondTerm;
            secondTerm = nextTerm;


        }


    }
}